#!/usr/bin/env python3
import argparse
import json
import networkx as nx
import numpy as np

# N[a,b] is the number of voters who prefer candidate a to candidate b
def load_pairwise_preferences_matrix(file):
    list_of_lists = json.load(file)
    n = np.stack([np.asarray(l, dtype=np.int8) for l in list_of_lists])
    return n

# As defined in section 4.12.1 https://arxiv.org/pdf/1804.02973.pdf,
# A Condorcet winner is an alternative a ∈ A that wins every head-to-head
# contest with some other alternative b ∈ A \ {a}.
def condorcet_winner(n, candidates):
    nb_candidates = len(candidates)
    for potential_winner in range(nb_candidates):
        won_all_contests = True
        for opponent in range(nb_candidates):
            if n[opponent][potential_winner] > n[potential_winner][opponent]:
                won_all_contests = False
        if won_all_contests:
            return candidates[potential_winner]
    return None

# As defined in section 4.12.1 https://arxiv.org/pdf/1804.02973.pdf,
# A weak Condorcet winner is an alternative a ∈ A that doesn’t lose any
# head-to-head contest with some other alternative b ∈ A \ {a}
def weak_condorcet_winners(n, candidates):
    weak_winners = []
    nb_candidates = len(candidates)
    for potential_winner in range(nb_candidates):
        lost_a_contest = False
        for opponent in range(nb_candidates):
            if n[opponent][potential_winner] > n[potential_winner][opponent]:
                lost_a_contest = True
        if not lost_a_contest:
            weak_winners.append(candidates[potential_winner])
    return weak_winners

# As defined in section 4 https://citizensassembly.arts.ubc.ca/resources/submissions/csharman-10_0409201706-143.pdf
# + other link strength methods defined in https://arxiv.org/pdf/1804.02973.pdf
def schulze_winners(n, candidates, link_strength_method='margin'):
    nb_candidates = len(candidates)

    if link_strength_method == 'margin':
        p = np.zeros((nb_candidates, nb_candidates), dtype=np.int8)
        for i in range(nb_candidates):
            for j in range(nb_candidates):
                if i != j:
                    p[i][j] = n[i][j] - n[j][i]
    elif link_strength_method == 'ratio':
        p = np.ones((nb_candidates, nb_candidates), dtype=np.int8) * np.Inf
        for i in range(nb_candidates):
            for j in range(nb_candidates):
                if i != j and n[j][i] > 0:
                    p[i][j] = n[i][j] / n[j][i]
    else:
        raise ValueError(f"link_strength_method '{link_strength_method}' is not implemented")

    for i in range(nb_candidates):
        for j in range(nb_candidates):
            if i != j:
                for k in range(nb_candidates):
                    if i != k and j != k:
                        s = min(p[j][i], p[i][k])
                        if p[j][k] < s:
                            p[j][k] = s

    winners = []
    for i in range(nb_candidates):
        has_lost_directly_or_indirectly = False
        for j in range(nb_candidates):
            if i != j:
                if p[j][i] > p[i][j]:
                    has_lost_directly_or_indirectly = True
        if not has_lost_directly_or_indirectly:
            winners.append(candidates[i])
    return winners

# from https://en.wikipedia.org/wiki/Schwartz_set (2023-07-14)
# In voting systems, the Schwartz set is the union of all Schwartz set components.
# A Schwartz set component is any non-empty set S of candidates such that
# Every candidate inside the set S is pairwise unbeaten by every candidate outside S; and
# No non-empty proper subset of S fulfills the first property.
def schwartz_set(graph, candidates):
    nb_nodes = 0
    candidate_to_beat_index = dict()
    beat_index_to_candidate = dict()
    for node in graph.nodes():
        candidate_to_beat_index[node] = nb_nodes
        beat_index_to_candidate[nb_nodes] = node
        nb_nodes += 1

    beats = np.zeros((nb_nodes, nb_nodes), dtype=bool)
    for (i, neighbors) in graph.adjacency():
        for j in neighbors:
            beats[candidate_to_beat_index[i]][candidate_to_beat_index[j]] = True

    for k in range(nb_nodes):
        for i in range(nb_nodes):
            for j in range(nb_nodes):
                beats[i][j] = beats[i][j] or (beats[i][k] and beats[k][j])

    schwartz_components = []
    for i in range(nb_nodes):
        schwartz_component = {i}
        is_schwartz = True
        for j in range(nb_nodes):
            if beats[j][i]:
                if beats[i][j]:
                    schwartz_component.add(j)
                else:
                    is_schwartz = False
        if is_schwartz:
            schwartz_components.append(schwartz_component)

    schwartz_set_index = set.union(*schwartz_components)
    return [beat_index_to_candidate[x] for x in schwartz_set_index]

# As defined in Appendix 4 of https://citizensassembly.arts.ubc.ca/resources/submissions/csharman-10_0409201706-143.pdf
def schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin'):
    nb_candidates = len(candidates)
    graph = nx.DiGraph()
    graph.add_nodes_from(candidates)

    for i in range(nb_candidates):
        for j in range(i, nb_candidates):
            margin = n[i][j] - n[j][i]
            if margin != 0:
                (x, y) = (i, j) if margin > 0 else (j, i)
                if link_strength_method == 'margin':
                    graph.add_edge(candidates[x], candidates[y], weight=abs(margin))
                elif link_strength_method == 'ratio':
                    graph.add_edge(candidates[x], candidates[y], weight=np.Inf if n[y][x] == 0 else n[x][y]/n[y][x])
                elif link_strength_method == 'winning_votes':
                    graph.add_edge(candidates[x], candidates[y], weight=n[x][y])
                elif link_strength_method == 'losing_votes':
                    graph.add_edge(candidates[x], candidates[y], weight=-n[y][x])
                else:
                    raise ValueError(f"link_strength_method='{link_strength_method}' is not implemented")

    while len(graph.nodes()) > 1:
        s_set = schwartz_set(graph, candidates)
        #print(f'Schwartz set: {s_set}')

        # eliminate all candidates that are not in the Schwartz set
        nodes_to_remove = []
        for candidate in graph.nodes():
            if candidate not in s_set:
                nodes_to_remove.append(candidate)
        if len(nodes_to_remove) > 0:
            #print(f'Removing nodes: {nodes_to_remove}')
            graph.remove_nodes_from(nodes_to_remove)

        if len(graph.nodes()) > 1 and len(graph.edges()) > 1:
            # remove all the edges that have the current minimal weight
            minimum_weight = np.Inf
            edges_per_weight = dict()
            for (i, neighbors) in graph.adjacency():
                for (neighbor, data) in neighbors.items():
                    weight = data['weight']
                    minimum_weight = min(minimum_weight, weight)
                    if weight in edges_per_weight:
                        edges_per_weight[weight].append((i, neighbor))
                    else:
                        edges_per_weight[weight] = [(i, neighbor)]

            #print(f'Removing edges of weight={minimum_weight}: {edges_per_weight[minimum_weight]}')
            for (i, j) in edges_per_weight[minimum_weight]:
                graph.remove_edge(i, j)
            continue
        break

    return [x for x in graph.nodes()]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Computes winners of the election')
    parser.add_argument('-c', '--candidates-file', help='candidates JSON file', type=argparse.FileType('r', encoding='utf-8'), required=True)
    parser.add_argument('matrix_file', help="pairwise preference matric JSON file", type=argparse.FileType('r', encoding='utf-8'))
    args = parser.parse_args()

    n = load_pairwise_preferences_matrix(args.matrix_file)
    assert(n.shape[0] > 0)
    assert(n.shape[0] == n.shape[1])

    candidates = json.load(args.candidates_file)
    assert(len(candidates) == n.shape[0])

    # Condorcet winner (win every head-to-head contest)
    c_winner = condorcet_winner(n, candidates)
    if c_winner is None:
        print('There is no Condorcet winner')
    else:
        print(f'Condorcet winner: {c_winner}')

    # Condorcet weak winner (do not lose any head-to-head contest)
    weak_c_winners = weak_condorcet_winners(n, candidates)
    if len(weak_c_winners) == 0:
        print('There are no weak Condorcet winners')
    else:
        print(f'Weak Condorcet winners: {weak_c_winners}')

    # Schulze winners (win every head-to-head contest, directly or indirectly)
    for method in ['margin', 'ratio']:
        s_winners = schulze_winners(n, candidates, link_strength_method=method)
        assert(len(s_winners) > 0)
        print(f'Schulze winners (beatpath, {method}): {s_winners}')

    # Schulze winners, computed via the Schwartz sequential dropping algorithm
    for method in ['margin', 'ratio', 'winning_votes', 'losing_votes']:
        s_winners = schwartz_sequential_dropping_winners(n, candidates, link_strength_method=method)
        assert(len(s_winners) > 0)
        print(f'Schulze winners (ssd, {method}): {s_winners}')
