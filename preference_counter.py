#!/usr/bin/env python3
import argparse
import json
import numpy as np
import pygraphviz as pgv
from itertools import groupby

def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)

def is_blank(ballot, unranked_value):
    for x in ballot:
        if x != 0 and x != unranked_value:
            return False
    return True

# ballot values should be consistent (unranked value should be the greatest)
# N[a,b] is the number of voters who prefer candidate a to candidate b
def pairwise_preferences(ballot, nb_candidates):
    n = np.zeros((nb_candidates, nb_candidates), dtype=np.int8)

    for a in range(nb_candidates):
        for b in range(a, nb_candidates):
            if ballot[a] < ballot[b]:
                n[a][b] = 1
            elif ballot[a] > ballot[b]:
                n[b][a] = 1
    return n

def pairwise_preferences_matrix(belenios_raw_data):
    # traverse data to compute some stuff and check some assumptions
    max_observed_value = -1
    nb_blank = 0
    min_nb_candidates = 1000
    max_nb_candidates = -1
    for ballot in belenios_raw_data:
        max_observed_value = max(max_observed_value, max(ballot))
        nb_candidates = len(ballot)
        min_nb_candidates = min(min_nb_candidates, nb_candidates)
        max_nb_candidates = max(max_nb_candidates, nb_candidates)
    assert(max_observed_value > -1)
    assert(min_nb_candidates == max_nb_candidates)
    nb_candidates = min_nb_candidates
    unranked_value = max_observed_value + 1

    # put a consistent value for unranked candidates
    fix_unranked_value = lambda x: unranked_value if x == 0 else x
    data = [ list(map(fix_unranked_value, ballot)) for ballot in belenios_raw_data ]

    # compute some stats
    nb_voters = len(data)
    nb_blank = sum([int(is_blank(ballot, unranked_value)) for ballot in data])

    # compute N, the matrix of pairwise preferences
    # N[a,b] is the number of voters who prefer candidate a to candidate b
    n = np.zeros((nb_candidates, nb_candidates), dtype=np.int8)
    for ballot in data:
        n += pairwise_preferences(ballot, nb_candidates)

    return (n, nb_candidates, nb_voters, nb_blank)

# compute graph of pairwise preferences
# When N[i,j] > N[j,i], then there is a link from candidate i to candidate j of strength N[i,j] – N[j,i]
def pairwise_preferences_graph(n, candidates):
    graph = pgv.AGraph(strict=True, directed=True)

    graph.add_nodes_from(candidates)

    assert n.shape[0] == n.shape[1]
    assert n.shape[0] == len(candidates)
    nb_candidates = len(candidates)
    for a in range(nb_candidates):
        for b in range(a, nb_candidates):
            margin = n[a][b] - n[b][a]
            if margin != 0:
                (x, y) = (a, b) if margin > 0 else (b, a)
                margin = abs(margin)
                cx = candidates[x]
                cy = candidates[y]
                text_fr = f"{cx} domine {cy} par {margin} votants\n{n[x][y]} votants préfèrent {cx} à {cy}\n{n[y][x]} votants préfèrent {cy} à {cx}"
                text_en = f"{cx} dominates {cy} by a margin of {margin} voters\n{n[x][y]} voters prefer {cx} over {cy}\n{n[y][x]} voters prefer {cy} over {cx}"
                graph.add_edge(cx, cy, label=f"{margin}", tooltip=f"{text_fr}\n\n{text_en}")

    return graph

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Computes pairwise preferences matrix and graph from belenios ballot files')
    parser.add_argument('-om', '--output-matrix-file', required=True)
    parser.add_argument('-og', '--output-graph-file', required=True)
    parser.add_argument('-c', '--candidates-file', help='candidates JSON file', type=argparse.FileType('r', encoding='utf-8'), required=True)
    parser.add_argument('ballot_file', nargs='+', help="belenios ballot JSON file", type=argparse.FileType('r', encoding='utf-8'))
    args = parser.parse_args()

    # compute the pairwise preference matrix from all belenios ballot files
    ballot_data = []
    for file in args.ballot_file:
        raw_data = json.load(file)
        (n, nb_candidates, nb_voters, nb_blank) = pairwise_preferences_matrix(raw_data)
        #print(n)
        ballot_data.append((n, nb_candidates, nb_voters, nb_blank))

    # make sure that the ballot fires are consistent with each other
    nb_candidates = [b[1] for b in ballot_data]
    assert(all_equal(nb_candidates))

    # aggregate values
    nb_voters = sum([b[2] for b in ballot_data])
    nb_blank = sum([b[3] for b in ballot_data])
    n = sum([b[0] for b in ballot_data])
    print(n)

    with open(args.output_matrix_file, 'w') as matrix_file:
        json.dump(n.tolist(), matrix_file)

    candidates = json.load(args.candidates_file)

    g = pairwise_preferences_graph(n, candidates)
    with open(args.output_graph_file, 'w') as graph_file:
        graph_file.write(g.to_string())

    #print(args)
    #raw_data = [[0,0,0,0,0,0],[1,2,3,4,5,5],[1,2,3,0,0,0],[0,0,0,0,0,0],[1,2,3,4,5,5]]

    #(n, nb_candidates, nb_voters, nb_blank) = pairwise_preferences_matrix(raw_data)
