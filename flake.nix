{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=23.05";
    flake-utils.url = "github:numtide/flake-utils";
    typst = {
      url = "github:typst/typst/main";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, typst }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs { inherit system; };
        typst-pkg = typst.packages.${system}.default;
        pyPkgs = pkgs.python3Packages;
      in rec {
        packages = {};
        devShells = {
          default = pkgs.mkShell {
            buildInputs = [
              pkgs.graphviz

              pyPkgs.ipython
              pyPkgs.numpy
              pyPkgs.pygraphviz
              pyPkgs.pytest
              pyPkgs.networkx

              typst-pkg
            ];
          };
        };
      }
    );
}
