#!/usr/bin/env python3
import numpy as np

import preference_counter
import elect

# all examples are from Section 3 of Markus Schulze's "The Schulze Method of Voting",
# version 13 (latest as I write these lines: 2023-07-14). https://arxiv.org/pdf/1804.02973.pdf

def test_example1():
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   4,   2,   3]] * 8 + \
                [[  2,   1,   4,   3]] * 2 + \
                [[  4,   3,   1,   2]] * 4 + \
                [[  3,   2,   4,   1]] * 4 + \
                [[  4,   3,   2,   1]] * 3

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0,  8, 14, 10],
                           [13,  0,  6,  2],
                           [ 7, 15,  0, 12],
                           [11, 19,  9,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 8+2+4+4+3
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['d']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['d']


def test_example2():
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   4,   2,   3]] * 3 + \
                [[  2,   1,   3,   4]] * 9 + \
                [[  3,   4,   1,   2]] * 8 + \
                [[  2,   3,   4,   1]] * 5 + \
                [[  4,   2,   3,   1]] * 5

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 16, 17, 12],
                           [14,  0, 19,  9],
                           [13, 11,  0, 20],
                           [18, 21, 10,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 3+9+8+5+5
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['c']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['c']


def test_example3():
    candidates = ['a', 'b', 'c', 'd', 'e']
    ballots =   [[  1,   3,   2,   5,   4]] * 5 + \
                [[  1,   5,   4,   2,   3]] * 5 + \
                [[  4,   1,   5,   3,   2]] * 8 + \
                [[  2,   3,   1,   5,   4]] * 3 + \
                [[  2,   4,   1,   5,   3]] * 7 + \
                [[  3,   2,   1,   4,   5]] * 2 + \
                [[  5,   4,   2,   1,   3]] * 7 + \
                [[  3,   2,   5,   4,   1]] * 8

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 20, 26, 30, 22],
                           [25,  0, 16, 33, 18],
                           [19, 29,  0, 17, 24],
                           [15, 12, 28,  0, 14],
                           [23, 27, 21, 31,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 5+5+8+3+7+2+7+8
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['e']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['e']


def test_example4():
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   2,   3,   4]] * 3 + \
                [[  4,   2,   1,   3]] * 2 + \
                [[  2,   3,   4,   1]] * 2 + \
                [[  4,   2,   3,   1]] * 2

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0,  5,  5,  3],
                           [ 4,  0,  7,  5],
                           [ 4,  2,  0,  5],
                           [ 6,  4,  4,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 3+2+2+2
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['b', 'd']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['b', 'd']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['b', 'd']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['b', 'd']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['b', 'd']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['b', 'd']


def test_example5():
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   2,   3,   4]] * 12 + \
                [[  1,   3,   4,   2]] * 6 + \
                [[  4,   1,   2,   3]] * 9 + \
                [[  3,   4,   1,   2]] * 15 + \
                [[  3,   2,   4,   1]] * 21

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 33, 39, 18],
                           [30,  0, 48, 21],
                           [24, 15,  0, 36],
                           [45, 42, 27,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 12+6+9+15+21
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['d']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['d']


def test_example6():
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   4,   2,   3]] * 8 + \
                [[  4,   1,   2,   3]] * 2 + \
                [[  3,   1,   4,   2]] * 3 + \
                [[  4,   2,   1,   3]] * 5 + \
                [[  4,   3,   1,   2]] * 1 + \
                [[  2,   3,   4,   1]] * 3 + \
                [[  3,   2,   4,   1]] * 1

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 11, 15,  8],
                           [12,  0,  9, 10],
                           [ 8, 14,  0, 16],
                           [15, 13,  7,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 8+2+3+5+1+3+1
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['a', 'c']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['a', 'c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['a', 'c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['a', 'c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['a', 'c']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['a', 'c']

def test_example7():
    # situation 1
    candidates = ['a', 'b', 'c', 'd', 'e', 'f']
    ballots =   [[  1,   4,   5,   2,   3,   6]] * 3 + \
                [[  6,   1,   4,   5,   3,   2]] * 3 + \
                [[  2,   3,   1,   5,   6,   4]] * 4 + \
                [[  6,   2,   3,   1,   4,   5]] * 1 + \
                [[  4,   5,   6,   1,   2,   3]] * 4 + \
                [[  6,   3,   2,   4,   1,   5]] * 2 + \
                [[  2,   5,   3,   4,   6,   1]] * 2

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 13,  9,  9,  9,  7],
                           [ 6,  0, 11,  9, 10, 13],
                           [10,  8,  0, 11,  7, 10],
                           [10, 10,  8,  0, 14, 10],
                           [10,  9, 12,  5,  0, 10],
                           [12,  6,  9,  9,  9,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 3+3+4+1+4+2+2
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['a']

    # situation 2
    candidates = ['a', 'b', 'c', 'd', 'e', 'f']
    ballots2 =  [[  1,   5,   4,   6,   2,   3]] * 2
    new_ballots = ballots + ballots2
    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(new_ballots)
    expected_n = np.array([[ 0, 15, 11, 11, 11,  9],
                           [ 6,  0, 11, 11, 10, 13],
                           [10, 10,  0, 13,  7, 10],
                           [10, 10,  8,  0, 14, 10],
                           [10, 11, 14,  7,  0, 12],
                           [12,  8, 11, 11,  9,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 3+3+4+1+4+2+2+2
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['d']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['d']


def test_example8():
    # situation 1
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   2,   4,   3]] * 3 + \
                [[  1,   3,   4,   2]] * 5 + \
                [[  1,   4,   3,   2]] * 1 + \
                [[  2,   1,   4,   3]] * 2 + \
                [[  4,   1,   3,   2]] * 2 + \
                [[  2,   3,   1,   4]] * 4 + \
                [[  3,   2,   1,   4]] * 6 + \
                [[  4,   2,   3,   1]] * 2 + \
                [[  3,   4,   2,   1]] * 5

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 18, 11, 21],
                           [12,  0, 14, 17],
                           [19, 16,  0, 10],
                           [ 9, 13, 20,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 3+5+1+2+2+4+6+2+5
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['a']

    # situation 2
    candidates = ['a', 'b', 'c', 'd', 'e']
    ballots =   [[  1,   2,   5,   3,   4]] * 3 + \
                [[  1,   4,   5,   2,   3]] * 5 + \
                [[  1,   5,   4,   2,   3]] * 1 + \
                [[  2,   1,   5,   3,   4]] * 2 + \
                [[  5,   1,   4,   2,   3]] * 2 + \
                [[  2,   3,   1,   4,   5]] * 4 + \
                [[  3,   2,   1,   4,   5]] * 6 + \
                [[  5,   2,   4,   1,   3]] * 2 + \
                [[  4,   5,   3,   1,   2]] * 5

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 18, 11, 21, 21],
                           [12,  0, 14, 17, 19],
                           [19, 16,  0, 10, 10],
                           [ 9, 13, 20,  0, 30],
                           [ 9, 11, 20,  0,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 3+5+1+2+2+4+6+2+5
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['b']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['b']


def test_example9():
    # situation 1
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   4,   2,   3]] * 5 + \
                [[  4,   1,   2,   3]] * 2 + \
                [[  3,   1,   4,   2]] * 4 + \
                [[  3,   4,   1,   2]] * 2

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0,  7,  9,  5],
                           [ 6,  0,  6,  6],
                           [ 4,  7,  0,  9],
                           [ 8,  7,  4,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 5+2+4+2
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['a']

    # situation 2
    candidates = ['a', 'b', 'c', 'd', 'e']
    ballots =   [[  1,   5,   3,   4,   2]] * 5 + \
                [[  4,   1,   2,   3,   5]] * 2 + \
                [[  3,   1,   5,   2,   4]] * 4 + \
                [[  3,   4,   1,   2,   5]] * 2

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0,  7,  9,  5, 13],
                           [ 6,  0,  6,  6,  8],
                           [ 4,  7,  0,  9,  4],
                           [ 8,  7,  4,  0,  8],
                           [ 0,  5,  9,  5,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 5+2+4+2
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['b']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['b']

def test_example10():
    candidates = ['a', 'b', 'c', 'd']
    ballots =   [[  1,   2,   3,   4]] * 6 + \
                [[  1,   1,   2,   2]] * 8 + \
                [[  1,   2,   1,   2]] * 8 + \
                [[  1,   3,   1,   2]] * 18 + \
                [[  1,   2,   1,   1]] * 8 + \
                [[  2,   1,   2,   2]] * 40 + \
                [[  4,   2,   1,   3]] * 4 + \
                [[  3,   4,   1,   2]] * 9 + \
                [[  2,   2,   1,   1]] * 8 + \
                [[  2,   3,   4,   1]] * 14 + \
                [[  4,   2,   3,   1]] * 11 + \
                [[  3,   4,   2,   1]] * 4

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 67, 28, 40],
                           [55,  0, 79, 58],
                           [36, 59,  0, 45],
                           [50, 72, 29,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 6+8+8+18+8+40+4+9+8+14+11+4
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['a']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['d']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['c']

def test_example11():
    candidates = ['a', 'b', 'c', 'd', 'e']
    ballots =   [[  1,   3,   5,   2,   4]] * 9 + \
                [[  3,   1,   2,   4,   5]] * 6 + \
                [[  5,   1,   2,   3,   4]] * 5 + \
                [[  5,   3,   1,   2,   4]] * 2 + \
                [[  5,   4,   3,   1,   2]] * 6 + \
                [[  2,   4,   3,   5,   1]] * 14 + \
                [[  3,   4,   2,   5,   1]] * 2 + \
                [[  3,   5,   4,   2,   1]] * 1

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 26, 24, 31, 15],
                           [19,  0, 20, 27, 22],
                           [21, 25,  0, 29, 13],
                           [14, 18, 16,  0, 28],
                           [30, 23, 32, 17,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 9+6+5+2+6+14+2+1
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['b']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['b']


def test_example12():
    candidates = ['a', 'b', 'c', 'd', 'e']
    ballots =   [[  1,   3,   5,   2,   4]] * 9 + \
                [[  2,   1,   3,   5,   4]] * 1 + \
                [[  3,   2,   1,   4,   5]] * 6 + \
                [[  5,   3,   1,   2,   4]] * 2 + \
                [[  4,   5,   1,   2,   3]] * 5 + \
                [[  4,   5,   3,   1,   2]] * 6 + \
                [[  3,   2,   4,   5,   1]] * 14 + \
                [[  4,   2,   3,   5,   1]] * 2

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[ 0, 20, 24, 32, 16],
                           [25,  0, 26, 23, 18],
                           [21, 19,  0, 30, 14],
                           [13, 22, 15,  0, 28],
                           [29, 27, 31, 17,  0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 9+1+6+2+5+6+14+2
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['e']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['e']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['e']


def test_example13():
    candidates = ['a', 'b', 'c']
    ballots =   [[  1,   2,   3]] * 2 + \
                [[  3,   1,   2]] * 2 + \
                [[  2,   3,   1]] * 1

    (n, nb_candidates, nb_voters, nb_blank) = preference_counter.pairwise_preferences_matrix(ballots)
    expected_n = np.array([[0, 3, 2],
                           [2, 0, 4],
                           [3, 1, 0]], dtype=np.int8)
    assert nb_candidates == len(candidates)
    assert nb_voters == 2+2+1
    assert nb_blank == 0
    assert np.array_equal(n, expected_n)

    assert elect.condorcet_winner(n, candidates) is None
    assert len(elect.weak_condorcet_winners(n, candidates)) == 0
    assert elect.schulze_winners(n, candidates, link_strength_method='margin') == ['a', 'b']
    assert elect.schulze_winners(n, candidates, link_strength_method='ratio') == ['a', 'b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='margin') == ['a', 'b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='ratio') == ['a', 'b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='winning_votes') == ['a', 'b']
    assert elect.schwartz_sequential_dropping_winners(n, candidates, link_strength_method='losing_votes') == ['a', 'b']
